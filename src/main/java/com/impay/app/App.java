package com.impay.app;

import com.impay.server.ServerRest;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class App extends AbstractVerticle {
	private JsonObject config = null;

	private static final Logger logger = LoggerFactory.getLogger(App.class);

	@Override
	public void start(Promise<Void> startPromise) {
		ConfigRetriever retriever = ConfigRetriever.create(vertx);
		retriever.getConfig(event -> {
			if (event.succeeded()) {
				config = event.result();
				logger.info("Config:\n{}", config.encodePrettily());
				config().mergeIn(config);
				deployVerticle(new ServerRest());
			}
		});
	}

	private void deployVerticle(AbstractVerticle vert) {
		vertx.deployVerticle(vert, new DeploymentOptions().setConfig(config())
			, res -> logger.error("Unable to deploy vertex of {}: {}", vert.getClass().getName(), res.cause().getMessage(), res.cause()));
	}

}

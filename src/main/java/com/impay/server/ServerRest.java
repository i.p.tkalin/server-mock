package com.impay.server;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerRest extends AbstractVerticle {
	private static final String AUTH_API_ENDPOINT = "/api/auth/v1/";

	private final Logger logger = LoggerFactory.getLogger(ServerRest.class);

	public ServerRest() {
	}

	@Override
	public void start(Promise<Void> startPromise) {
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());

		JsonArray endpoints = config().getJsonArray("endpoints");

		endpoints.forEach(obj -> {
			final JsonObject json = (JsonObject)obj;
			final JsonObject request = json.getJsonObject("request");
			final JsonObject response = json.getJsonObject("response");
			if (request.containsKey("pathRegExp")) {
				router
					.routeWithRegex(HttpMethod.valueOf(request.getString("method")), request.getString("pathRegExp"))
					.handler(getRequestHandler(response));
			} else {
				router
					.route(HttpMethod.valueOf(request.getString("method")), request.getString("path"))
					.handler(getRequestHandler(response));
			}
		});

		vertx.createHttpServer().requestHandler(router).listen(config().getInteger("port"));
	}

	private String procTemplates(String template, RoutingContext ctx) {
		Pattern re = Pattern.compile("\\{\\{(param|query|body):([^}]+)}}");
		Matcher ma = re.matcher(template);
		while (ma.find()) {
			String paramType = ma.group(1);
			String paramId = ma.group(2);
			String paramValue = "";
			switch (paramType) {
				case "param":
					paramValue = ctx.request().getParam(paramId);
					break;
				case "query":
					List<String> paramValues = ctx.queryParam(paramId);
					if (paramValues != null && paramValues.size() > 0)
						paramValue = paramValues.get(0);
					break;
				case "body":
					Object val = ctx.getBodyAsJson();
					String[] names = paramId.split("\\.");
					for (int i = 0; i < names.length && val != null; i++) {
						val = ((JsonObject)val).getValue(names[i]);
					}
					if (val != null) {
						paramValue = val.toString();
					}
					break;
			}
			template = ma.replaceFirst(paramValue);
			ma = re.matcher(template);
		}
		return template;
	}

	private Handler<RoutingContext> getRequestHandler(JsonObject response) {
		return ctx -> {
			ctx.response().setStatusCode(response.getInteger("status"));
			if (response.containsKey("message")) {
				ctx.response().setStatusMessage(response.getString("message"));
			}
			if (response.containsKey("headers")) {
				JsonArray headers = response.getJsonArray("headers");
				headers.forEach(hdr -> {
					JsonObject h = (JsonObject)hdr;
					ctx.response().headers().set(h.getString("name"), h.getString("value"));
				});
			}
			if (response.containsKey("body")) {
				Object body = response.getValue("body");
				String bodyStr;
				if (body instanceof JsonObject) {
					bodyStr = ((JsonObject)body).encodePrettily();
				} else if (body instanceof JsonArray) {
					bodyStr = ((JsonArray)body).encodePrettily();
				} else {
					bodyStr = body.toString();
				}
				ctx.response().end(procTemplates(bodyStr, ctx));
			} else {
				ctx.response().end();
			}
		};
	}

}

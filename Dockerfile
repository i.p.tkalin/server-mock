FROM java:8-jre

ENV VERTICLE_HOME /usr/verticles

# jar
ENV VERTICLE_FILE server-mock.jar

# app config file
ENV VERTX_CONFIG_PATH config.json

# port
EXPOSE 9997

# copy jar to container
COPY target/$VERTICLE_FILE $VERTICLE_HOME/

VOLUME ["/applogs"]

# launch
WORKDIR $VERTICLE_HOME
CMD exec java -Xms2048m -Xmx8192m -Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.Log4j2LogDelegateFactory -Dlog4j.configurationFile=log4j2.xml -jar $VERTICLE_FILE
